console.log("Hello World");

/*
	1. Create variables to store to the following user details:

	-first name - String
	-last name - String
	-age - Number
	-hobbies - Array
	-work address - Object

		-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	//Add your variables and console log for objective 1 here:
	
	let firstName = "Db";
	let lastName = "Villacampa";
	let age = 24;
	let hobbies = ["Playing online games", "Volleyball", "Cooking", "Eating"];
	let workAddress = {
		houseNumber: 123,
		street: "Magsaysay",
		city: "Norzagaray",
		state: "Bulacan"
	};

	console.log("First name: " + firstName);
	console.log("Last name: " + lastName);
	console.log("Age: " + age);
	console.log("Hobbies:");
	console.log(hobbies);
	console.log("Work Address:");
	console.log(workAddress);



/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	

	let fullName = "Steve Rogers";
	console.log("My full name is: " + fullName);

	let currentAge = 40;
	console.log("My current age is: " + currentAge);
	
	let friends = ["Tony", "Bruce", "Thor", "Natasha", "Clint", "Nick"];
	console.log("My Friends are: ");
	console.log(friends);
	

	let profile = {

		username: "captain_america",
		name: "Steve Rogers",
		age: 40,
		isActive: false,

	};
	console.log("My Full Profile: ");
	console.log(profile);

	let name = "Bucky Barnes";
	console.log("My bestfriend is: " + name);

	const lastLocation = "Arctic Ocean";
	//lastLocation = "Atlantic Ocean";
	console.log("I was found frozen in: " + lastLocation);

